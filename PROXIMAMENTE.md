---
layout: page
title: "PROXIMAMENTE"
permalink: /next/
image: images/ojo.jpg
comments: true 
---

![#piloto]({{ site.baseurl }}/images/ojo.jpg)


**TEMPORADA ÚNICA 2023**

La Mcs. Lupita Amaya entrevistará a todos los y las candidatas a la Asamblea Nacional 2023 que sientan oportuno someterse a las interrogantes que un habitante peninsular le haría.

**Agradecemos a las siguientes personas por su participación:**

1) **Arisdely Parrales** 

2) **Fernando Vargas** 

3) **María Auxiliadora Quiroz** 

4) **Melina Villacrés** 

5) **Vanessa Córoba** 

