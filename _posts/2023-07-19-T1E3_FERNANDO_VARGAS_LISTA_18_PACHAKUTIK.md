---
layout: post  
title: "T1E3 FERNANDO VARGAS LISTA 18 PACHAKUTIK"  
date: 2023-07-19  
categories: podcast  
image: images/T1E3_CHOLING_FERNANDO_VARGAS.jpg
image2: https://choling.gitlab.io/images/T1E3_CHOLING_FERNANDO_VARGAS_002.jpg
podcast_link: ia802708.us.archive.org/22/items/t-1-e-3-choling-fernando-vargas/T1E3_CHOLING_FERNANDO_VARGAS.mp3
tags: [Fernando Vargas, Pachakutik, Yaku Pérez, Asamblea Nacional, Muerte cruzada]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E3_CHOLING_FERNANDO_VARGAS.jpg)

<audio controls>
    <source src="https://ia802708.us.archive.org/22/items/t-1-e-3-choling-fernando-vargas/T1E3_CHOLING_FERNANDO_VARGAS.mp3" type="audio/mpeg">
    <script async data-type="track" data-hash="ePDBOReWZ5" data-track="3" src="https://app.fusebox.fm/embed/player.js"></script>
</audio>

**T1E3 FERNANDO VARGAS LISTA 18 PACHAKUTIK**

**TRANSCRIPCION**

**Lupita Amaya:** Buenos días, Don Fernando. Es un gusto para nosotros tenerlo aquí. Queremos aprovechar esta oportunidad, estos minutos que tenemos para compartir y escucharlo, escucharlo para que la comunidad nuestra de la provincia de Santa Elena se haga eco de sus propuestas, se haga eco de su voz, de su pensamiento para el bienestar de toda nuestra península. El día de hoy tenemos a Don Fernando Vargas, quien es candidato por Pachakutik. Bienvenido. Me gustaría en primer lugar que usted me diga por qué querer ser asambleísta. 

**Fernando Vargas:** Muchas gracias, doctora Lupita Amaya Jacho, por la invitación. Al señor técnico, un gusto de conocerlo. Y a toda la familia Maya, qué gusto de verlo, a Miguelito, a Fernando, a Patricio. Y a toda la familia Amaya, que son amigos de hace años. Y cuando estuve de presidente de la Cámara, ellos siempre estaban al lado mío. Bueno, su pregunta mi doctora, y buenos días peninsulares y buenos días a Ecuador, porque este programa tiene varias plataformas. Bueno, ¿Por qué quiero ser yo asambleísta? Sencillo para poder corregir las fallas que tiene esta provincia casi al 90% de una desnutrición infantil que supera el 40%. Y los proyectos productivos realmente no caminan en la provincia de Santa Elena. Y yo como empresario pesquero, estoy jubilado, pero realmente he estado más de 53 años viviendo aquí en la provincia, antes península de Santa Elena, y he estado dedicado a la pesca industrial, a las fábricas de harina, enlatados, barcos, después camaroneras, laboratorio larvas. Eso ha sido mi función y veo realmente que la provincia no mejora en su parte productiva. 

**Lupita Amaya:** Don Fernando, como asambleísta desde la su curul allá en Quito, ¿Usted cree que podría incidir en el cambio económico aquí en la provincia considerando que el periodo legislativo es sumamente corto, es un periodo acortado? Y no sé qué tan factible sea lograr todos esos proyectos, porque son grandes proyectos que usted tiene, para poder hacer. ¿Usted cree factible que se puede incidir en eso? 

**Fernando Vargas:** Por supuesto. Tenemos 16 meses, pero tengo una gran ventaja. En la ley de pesca en que vamos a entrar nosotros para que regresen al mar 7.000 pescadores. Nosotros, como les decía, somos hombres de mar y hemos ayudado al resto asambleísta dando nuestros artículos para la nueva ley de pesca. Ya realmente algunos no han funcionado porque la esencia que yo mandé a los artículos no lo hicieron como yo… Debería de hacer. Entonces nosotros vamos con una propuesta clarísima. Nosotros vamos con este proyecto de ley para que las empresas calificadas, especializadas, de guardianía, de seguridad... se firme un convenio de cooperación con las cooperativas pesqueras. 

**Lupita Amaya:** O sea, esa ley y ese proyecto de ley ya está en el Congreso o recién usted lo llevaría para allá la ley de pesca? 

**Fernando Vargas:** Yo lo llevo, pero sí conocen a algunos asambleístas porque han hablado conmigo y conocen. Lamentablemente, en estos ocho meses de ellos no hubo tiempo, pero eso prácticamente algunos asambleístas lo conocen y más que toda la gente de la ciudad de Quito que me apoyan en los ministerios. 

**Lupita Amaya:** O sea, este proyecto de ley de pesca, ¿usted lo hizo conocer a las personas anterior, que nos representaron en el periodo anterior y no avanzaron, no se quedaron solamente allí? ¿Y su intención ahora es darle vida a hacer que este proyecto sea aprobado? 

**Fernando Vargas:** Definitivamente, definitivamente, porque es un proyecto sencillo y directo y lo he cogido con la experiencia que he tenido como jefe político de Salinas. Yo traje al GOE y al GIR, y nosotros hicimos una estrategia de seguridad en alta mar y creamos que en tres meses bajamos los robos de motores. Yo le brindé la seguridad humana en el mar y eso tengo algo de experiencia y por eso este proyecto que realmente lo vamos a socializar, vamos a hablar con la mayoría de los asambleístas y esto va a favorecer a toda la costa ecuatoriana. Estoy seguro, completamente seguro que se lo va a probar porque con eso controlamos el robo como lo hice. 

**Lupita Amaya:** Dentro de esta ley de pesca usted dice que también se incluye la seguridad en alta mar, una aspiración de todos los pescadores, porque los pescadores día a día vemos de que desaparecen, que les roban los motores, que les dejan sin los medios. Hemos visto mucha gente quedarse sin los jefes de hogar inclusive. Entonces, me parece que sería un excelente proyecto el que se trabaje por una ley de pesca acá, que es de lo que vive la península, ¿no? 

**Fernando Vargas:** Definitivamente. Nosotros lo queríamos hacer, incorporar a la nueva ley de pesca con una reforma esta ley nuestra, palpado de los problemas nuestros en la provincia de Santa Elena. 

**Lupita Amaya:** Ahora, ¿usted cree que hay el tiempo como para que vaya primer debate, segundo debate, sea aprobado, se hallen los consensos? ¿Usted cree que sea posible en este tiempo? 

**Fernando Vargas:** Conociéndome como soy yo, un hombre de gestión, tengo un diplomado de planificación, de estrategia, conozco porque realmente no solamente usted va a presentar eso en asamblea, sino que las comisiones que se formen, yo las voy a visitar porque eso no prohíbe al asambleísta. Yo voy a estar perennemente en las comisiones para poder empujar este pequeño proyecto que no es de tanto estudio. Simple: una entre las guardias de especialidad y seguridad, que ahora está permitido portar armas, y nuestras cooperativas pesqueras. Yo lo veo fácil en estos 16 meses, muy optimista, para que se ha incorporado la ley de espejo. Independientemente de esto, también yo ya estoy haciendo contactos. No me siento ganador, pero lo hice por la amistad con el comandante de la Marina, con sede en Quito, para saber exactamente por qué es que no tienen el presupuesto necesario para darle la seguridad a nuestros pescadores. 

**Lupita Amaya:** Ahora, nosotros conocemos que estamos ahorita en una situación de muerte cruzada, precisamente por esa falta de digamos, posibilidad de establecer convenios, contactos, de coordinar ideas. Y todo en la Asamblea se volvió ingobernable. Vemos cómo el gobierno terminó haciendo algo que... vamos a ver en la vida y en el futuro qué dice la historia de esto que ha pasado en el país. Sin embargo, ¿cómo hacer para que, por ejemplo, usted es un hombre así como vibrante, como de muchas ideas, pueda conciliar para que los otros grupos, me salgo ya de la alianza Pachakutik, usted tiene su ley, ¿Cómo hacer para que los otros se adhieran a usted y logren alcanzar ideas? ¿O cómo haría usted también para que, si presentan otras personas las ideas, proyectos, usted se sume para dar gobernabilidad al país? Porque realmente en este próximo período necesitamos que haya gobernabilidad. 

**Fernando Vargas:** Definitivamente. Si no, fíjese usted cómo estamos retrocediendo en la parte económica. Tenemos elevado los índices en la parte de los créditos internacionales de que no somos sujeto de crédito. Es sencillo. Yo lo que planteo es realmente, en este momento de inseguridad, más que todo en el mar y también en tierra, sea acogida este proyecto. Y si no hay tiempo, tendré entonces que empujar lo que sucede en la Comandancia de la Marina para que realmente nos dé la protección que no nos está dando en este momento. Las lanchas rápidas no tienen combustible. Yo me acuerdo que traje dos de San Eduardo y yo mismo buscaba el financiamiento del combustible y también traes a yo una fragata, me acuerdo tanto de Esmeraldas, todo es gestión. Entonces tenemos un plan A y plan B en todos los tres proyectos que vamos a presentar. 

**Lupita Amaya:**¿Y cuál sería el otro proyecto? Ya tenemos el de ley de pesca, ¿cuál es el otro? 

**Fernando Vargas:** Muy bien. Bueno, yo soy un hombre que conozco bien las comunas, ya estoy diez años visitándolo en los fines de semana y me atrevo a decir, doctora, que soy el que más conozco el asunto del trasvase de las aguas de Daule Peripa hasta nuestra represa. 

**Lupita Amaya:** De San Rafael. 

**Fernando Vargas:** Yo sí, hasta la represa de San Vicente, la represa del azúcar con los canales de San Rafael para potabilizarla por un lado y por el otro para la agricultura, porque ese fue el multipropósito cuando se hizo el trasvase. Yo conozco la problemática de los comuneros y me he reunido con ellos, no ahora, tiempo atrás, sin estar en mente ser candidato a Asambleísta. Yo hago las cosas sin pensar en la política. Entonces ya tengo adelantado, por ejemplo, las conversaciones con los gobiernos amigos y ya me contestó en el mes de octubre del año pasado un gobierno. No quiero decir el nombre porque aquí todo le copian. Entonces para los préstamos no reembolsables a estos pequeños agricultores comuneros. Lo primero que me dijeron es que haga la ficha técnica y ya tenemos lista la ficha técnica un poquito más allá de 1500 hectáreas. Vamos a espacio porque esto, fíjese usted, tantos años votado.

**Lupita Amaya:**¿Cómo se llamaría esa ley? 

**Fernando Vargas:** Ya, esa es la ley, nosotros le pusimos todo de agricultura para el fomento productivo de los comuneros. 

**Lupita Amaya:**¿Ya? 

**Fernando Vargas:** Eso es, con préstamos no reembolsables con este gobierno amigo, que vamos paso a paso y ya terminamos las fichas técnicas. Van a mandar un técnico de parte de este gobierno para que vean la calidad de la tierra, la calidad del agua.

**Lupita Amaya:** Yo leí en la mañana en su Facebook que tenía una publicación en la que usted decía que iba a fiscalizar a PIDASE, que iba a fiscalizar a todos los municipios, etc. Pero como estamos hablando de agricultura, sí me gustaría escucharle si usted vio bien el trabajo de PIDASE. Por ejemplo, PIDASE hizo un trabajo, al menos desde el punto de vista de nosotros, interesante, ¿no? En el sector de San Vicente, San Rafael, todo ese sector. ¿Cómo, cuál es su opinión? Me estoy saliendo un poquito del tema, pero lo leí hoy día en su Facebook.

**Fernando Vargas:** Si como no... Justamente doctora, ayer que estuve en la comuna Sube y Baja, aunque siempre los visito, ¿No? Pero esta comuna no la visitaba ya hace dos años. Y me encuentro que está peor de la que lo vi. Y no han hecho absolutamente nada. Están hablando de 14 kilómetros de la parroquia de Julio Moreno hasta la comuna. Yo le puedo resumir el asunto PIDASE en pocas palabras: un fracaso total. Primero, porque no se conoce exactamente cuánta fue la inversión millonaria. Segundo, cómo puede hacer los silos en una zona que no es maicera. Vichingota no es maicera y han hecho esa inversión de más de 5 millones 500 mil dólares que no sirve para nada. 

**Lupita Amaya:**¿Cómo hacer para que su ley? la ley que usted quiere impulsar, que es la ley de la agricultura, ¿Cómo hacer para que usted, para que no suceda lo mismo? Porque vemos que los proyectos a veces se van en un saco roto, y al instante se ven muy lindos, pero luego desaparecen. 

**Fernando Vargas:** Sí sencillo, es sencillo. Con este gobierno, amigo, porque estoy con dos más, que todavía no me contestan, es sencillo. Ellos mandan los técnicos, califica los préstamos no reembolsables, ven si estamos cerca del agua, capacitan, buscan buena semilla, supervisan todo lo que es el cultivo, la cosecha, el almacenamiento y los precios internacionales para vendérselo a ellos mismos. Nosotros no nos vamos a ir de muchas cosas de estos programas que han sido un fracaso, nosotros vamos directamente al comunero pequeño agricultor, nada más. Nosotros no vamos a estos agricultores grandes en absoluto. 

**Lupita Amaya:**¿Y la tercera ley? Estoy con inquietud de escuchar en la tercera ley, claro. 

**Fernando Vargas:** Mucho gusto. Bueno, esta ley, este proyecto de ley yo ya lo presenté hace más de tres años, que es la reactivación del sector turístico. Y está en la Corte Constitucional porque me indicaron para reabrir los casinos vía consulta popular, porque eso lo hizo el presidente Correa. Que cerrar los casinos vía consulta se lo puede reabrir vía consulta. Pero la corte constitucional me dice que tengo que hacer una reforma al COIP y por eso tengo paralizado. Este proyecto ya está, ya hay conocimiento de las altas autoridades de nuestro país. Ahora lo que tengo que hacer es la asamblea. Imagínense impulsarla. Y a mí me gusta hacer eso. Yo toda mi vida, como todo el mundo me conoce, lo más difícil lo hago fácil y lo fácil lo hago enseguida para que sea de provecho para los peninsulares. 

**Lupita Amaya:** Muy bien. Ahora, a propósito de lo difícil, tenemos nosotros una asamblea desprestigiada, desprestigiada, desprestigiada. ¿Cómo hacer para que, desde su persona, desde su actitud, llevar hacia el mejoramiento de la imagen de la asamblea? 

**Fernando Vargas:** Bueno, aquí está una parte importante que el 20 de agosto todos los ecuatorianos debemos ya saber por quién votar. Específicamente en nuestra provincia, que la conozco los 3670 km² de superficie como la palma de mi mano, ya dejémonos de estar oyendo a estos políticos su promesa que son falsas todas, porque ahí están los resultados negativos, ciento por ciento. Y nuestro pueblo ya debe dejar de ser complaciente y buscar realmente nuevos líderes para sacar adelante la provincia. Nosotros vamos a estar en lo mismo con los políticos de siempre.
 
**Lupita Amaya:**¿Por qué cree que cuando un asambleísta de la península, porque ya hemos tenido muchos ya en la asamblea, por qué cree que desaparecen cuando nosotros hemos tenido representantes de la radio, de universidades, de toda la excepción de uno? ¿Por qué cree que ya se invisibilizan? Se desaparecen, es como que la península o la provincia no tuviesen un solo representante. ¿A quién cree que se debe eso? 

**Fernando Vargas:** Sencillo también, mi doctora, por la vergüenza. El problema es que van sin conocimiento. Son gente que no son de empresa. Nosotros, los empresarios o los que hemos sido gerentes de grandes empresas, tenemos otra visión, o otra misión de lo que es una inversión con resultados, con ganancias. Estas personas que han ido a la Asamblea ni siquiera han administrado, como dijo alguien por ahí, una tienda de una esquina. No saben de proyectos, de préstamos, no saben de las entidades internacionales que nos pueden dar crédito. No conocen. Como usted bien dice, han sido de radio, han sido de la universidad, pero lamentablemente no son prácticos como nosotros, que somos empresarios de lucha que nos endeudamos para sacar adelante una empresa grande. ¿Verdad? y dar trabajos y ganar. Nosotros, por ejemplo, mi doctora, para no olvidarnos del tema de las empresas, nosotros fuimos los primeros que hicimos unos contratos de asociaciones lícitos con la compañía Ushida Corporation de Japón y la compañía Dukso de Corea. Acuérdense en la década del 70, 80, 90, que estábamos llenos de chinos. Eso fue una gestión que hicimos nosotros. Todo el mundo ganó plata nosotros, los trabajadores, los tripulantes y el estado ecuatoriano que nos cobraba, me acuerdo, en ese entonces casi como 400 dólares la tonelada cuando exportamos la pesca congelada a estos países. 

**Lupita Amaya:** Sabemos que un legislador, un asambleísta no maneja presupuestos, ¿verdad? 

**Fernando Vargas:** Por supuesto. 

**Lupita Amaya:** Maneja es la posibilidad de aprobarse leyes. Entonces, usted tiene tres proyectos que más o menos uno es más viable que otro y que serán objeto suyo de impulsarlos, lograr que pasen los debates respectivos y ojalá tener como península, como provincia, al menos una de estas leyes que me parecen interesantes. La ley de pesca beneficiaría nuestra península, la ley de la agricultura igualmente, porque tenemos el trasvase, y la reactivación del sector turístico, por supuesto, con mi debida opinión que me la guardo al respecto. Pero bueno. 

**Fernando Vargas:** Pero al casino van gente de billetes, gente turista. 

**Lupita Amaya:** Si yo no voy. 

**Fernando Vargas:** O sea, usted viaja. Yo también viajo cuando tengo la posibilidad de hacer. 

**Lupita Amaya:** No, pero yo no voy al casino, le digo. 

**Fernando Vargas:** Sí, pero eso viene en los turistas internacionales. 

**Lupita Amaya:** Les gusta, sí. 

**Fernando Vargas:** Yo estoy hablando aquí, pues ustedes saben que somos chiros, pero van a ser de hoteles tres, cuatro, cinco estrellas hacia allá. Vean ustedes cuando sacamos nosotros ese proyecto, cuántos inversionistas del Perú, de Chile, querían venir y les gustó la idea mía. Lamentablemente tenemos ese problema legal. 

**Lupita Amaya:** Ahora una cosa. ¿Qué tan orgánico podrá ser usted? ¿O sea, qué tan obediente podría ser? Por ejemplo, se me ocurre decir Yaku Pérez es presidente y da una orden, cualquier orden. ¿Y qué tan orgánico, como se usa ahora la palabra, va a ser usted tan obediente al respecto? ¿O va a ser usted crítico? ¿O va a ser una persona que dice no, no, yo voy a pongo mi opinión y voy a tomar la decisión correcta? ¿No terminará siendo independiente? 

**Fernando Vargas:** Bueno, es una muy buena pregunta y yo mismo me la he hecho. Yo soy un hombre orgánico. Yo sigo, pues como usted sabe, yo tengo que ponerme en primer ejemplo. Yo sigo gerente, inversionista, etcétera. Y me gusta la parte orgánica. Si usted da órdenes, significa que usted también recibe órdenes. Y me encanta eso, la coordinación. No se olvide que yo también me he sido presidente clasista. Y siempre yo he coordinado con el resto de los directores. Sin embargo, si hay alguna orden que va en contra de los intereses de la provincia de Santa Elena, no lo va a aceptar. Todo lo va a hacer siempre que gane la provincia de Santa Elena, siempre. Y ese asunto del trasvase, antes que se nos vaya, yo voy a fiscalizarlo, porque realmente con una inversión de 2.000 millones de dólares, apenas el 45% de los peninsulares tomamos agua potable. Y el resto de lo que es la agricultura, de las 40.000 hectáreas que tenemos cerca de los canales, solamente 3.000 hectáreas están en cultivo permanente. ¿Qué significa eso? Significa que somos el 0.5% de la producción nacional y Santo Domingo de los Tsáchidas, que nació junto a nosotros, ellos el 6, nosotros el 7 de noviembre del 2007, tiene 50.000 hectáreas y las 50.000 en cultivo permanente. Nosotros como provincia mi doctora, hemos fracasado, hemos retrocedido. Todo quebrado. Y hacia allá vamos, por lo menos estos 16 meses que nos escuchen y presentamos. Y si se viene después del 2025 la reelección, ahí estaremos nuevamente para que nuestros proyectos sean... 

**Lupita Amaya:** Don Fernando. 

**Fernando Vargas:** Sean de buen resultado. 

**Lupita Amaya:** Dígale a la gente, dígale a la gente, a los electores. Dígale por qué votar por usted. 

**Fernando Vargas:** Bueno, realmente yo he visto el resto de candidatos con todo el respeto. Yo admiro y respeto a las damas. Yo nunca peleo políticamente con las damas. Yo peleo con estos políticos que todos conocemos. Les he dicho, vamos a un debate porque ahora no se puede hacer debate porque no lo permite la ley de elecciones de la nueva democracia. Les he dicho a las radios, hagan un debate. Yo quise debatir con Patricio Cisneros, quiero debatir con Lenín Mera y con este señor Otto Vera, a los tres independientemente. Y si no quieren ir independiente, van a ir los tres juntos, vayan con barra propia, vayan con sus moderadores, para que la provincia sepa los problemas que existen. Y no hay solución en nada, en absoluto. A mí es el único que no me puede mentir porque yo estoy allá, yo estoy allá en el terreno y veo que esto no avanza, esto retrocede. Y si hemos tenido plata, doctora, en estos 15 más años de provincia, hemos tenido dinero. Hemos recibido a los tres municipios y la prefectura 1.950 millones de dólares, de los cuales sabemos que el 70% son para obras. Hemos recibido entonces 1.365 millones de dólares, aproximadamente, para obras. ¿Y cuáles son las obras? No hay una sola obra de infraestructura. Todas son de pacotilla. 

**Lupita Amaya:** Entonces, hemos escuchado a don Fernando Vargas y él nos está pidiendo su voto. Como estamos en un podcast, el podcast Chóling. Nosotros estamos en este momento haciendo que usted conozca su candidato. Queremos también pedirle a usted que lo escuche, que lo reflexione y desearle a usted, don Fernando, éxitos. Éxitos en esta jornada que tenemos de campaña, en esta jornada de elecciones. Y ojalá pues en esta oportunidad ustedes sean asambleístas no. 

**Fernando Vargas:** Muchísimas gracias. Y me faltó rapidito porque esto es corto tiempo, esta entrevista. Dos cosas. Nosotros vamos a ir con dos propuestas clarísimas. No proyectos leyes, sino propuestas que vamos. 

**Lupita Amaya:** Ya. 

**Fernando Vargas:** Que vamos a accionar en Quito porque no se olvide de que nosotros accionamos la construcción del Hospital Liborio Panchana y no sabemos realmente cómo es el trámite. Y vamos nosotros realmente a proponer que se construya una maternidad aquí en la Proyección Santa Elena que no tenemos. Y también en el asunto de seguridad, vamos a ir a los ministerios respectivos de Justicia, de Gobierno, para que sepan por qué tenemos este problema de sicariato, piraterismo y todo este tráfico de drogas. Se lo vamos a conversar para que realmente tengamos nosotros una solución a estos graves problemas. 

**Lupita Amaya:** Bueno, le deseamos todos los éxitos y esperamos, como le dije hace un momento, que... En esta oportunidad, Fernando sea asambleísta. Éxitos.

**Fernando Vargas:** Muchísimas gracias, doctora. Usted también, señor... .

**Lupita Amaya:** Villagrán. 

**Fernando Vargas:** Villagrán, mucho gusto. Yo pensé que era una hora, porque estoy acostumbrado a las entrevistas de  una hora, pero este es un programa mucho más técnico para que llegue a miles de personas. Peninsulares, para ya despedirme. Voten por un cambio, no voten por lo mismo de siempre. Las propuestas mías son claras, las propuestas mías son viables. Son 16 meses, por ustedes y verán ustedes los resultados. Yo soy un hombre de palabra y mi palabra es ley. Y si no terminamos los 16 meses, en su mano está para que me reelijan. Si tenemos todavía todo el futuro, todo el horizonte para salir adelante, mar, tierra, agricultura, turismo, y somos pobres, imagínese. Buenos días, doctora. 

**Lupita Amaya:** Muchas gracias.



+ Correo: <unicornioazul@disroot.org>
