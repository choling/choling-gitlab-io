---
layout: post  
title: "T1E1 LA ANFITRIONA: LUPITA AMAYA JACHO"  
date: 2023-06-29  
categories: podcast  
image: images/LUPITA_AMAYA.jpg
image2: https://choling.gitlab.io/images/T1E1_CHOLING_LUPITA_AMAYA_002.jpg
podcast_link: ia902602.us.archive.org/6/items/choling-t-1-e-1-la-anfitriona-lupita-amaya-jacho-b/CHOLING_T1E1_LA_ANFITRIONA_LUPITA_AMAYA_JACHO_b.mp3
tags: [Lupita Amaya, Revolución Ciudadana, Rafael Correa, Asamblea Nacional, Muerte cruzada]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/LUPITA_AMAYA.jpg)

<audio controls>
    <source src="https://ia902602.us.archive.org/6/items/choling-t-1-e-1-la-anfitriona-lupita-amaya-jacho-b/CHOLING_T1E1_LA_ANFITRIONA_LUPITA_AMAYA_JACHO_b.mp3" type="audio/mpeg">
    <script async data-type="track" data-hash="ePDBOReWZ5" data-track="3" src="https://app.fusebox.fm/embed/player.js"></script>
</audio>

**TRANSCRIPCION**

**Anfitrión de oportunidad:** ¿Cómo se te ocurrió la idea de hacer el podcast de Choling, Entrevistas Políticas para la Asamblea 2023? 

**Lupita Amaya:** Esta asamblea es sui géneris, ¿Verdad? Porque viene de una manera imprevista, porque tiene muy poco tiempo para conocer a los candidatos, porque va a tener muy tiempo para la legislación propiamente. Entonces me llené de dudas y de inquietudes que fueron confirmadas con la palabra de mi padre que me decía que no sabía por quién sufragar. En ese marco pues decidí que yo también debía conocer a los candidatos, que no podíamos llegar al día de las elecciones a una papeleta donde todos son unos desconocidos. Entonces, eso me motivó para poder decidir, buscar la forma de conocerlo y qué mejor a través de un podcast. 

**Anfitrión de oportunidad:** Bueno, pero este podcast es una búsqueda en donde tú buscas información para saber por quién votar, para ayudar a tu padre a tomar una elección. Son diez candidatos que hay, de los cuales hay tres que son conocidos y el resto no tanto. Me imagino que tienes tus preferencias como el lógico. ¿Qué sentido tiene entrevistar a los candidatos que no son de tu preferencia? 

**Lupita Amaya:** Estoy en este momento haciendo una reflexión porque claro, todo el mundo tiene su tendencia política. Y yo la tengo. Sin embargo, quisiera yo no elegir en base a la tendencia política que uno le marca, sino más bien en base al conocimiento del candidato. Porque de pronto resulta que tenemos candidatos que van a hacer un papel importante en la asamblea, que no lo han hecho en asambleas anteriores, en el Congreso, gente que ha tenido trayectoria y que van allá y se invisibilizan. 

**Anfitrión de oportunidad:** ¿Cómo crees tú que es una entrevista exitosa? Desde el punto de vista de Choling Entrevistas Políticas 2023. 

**Lupita Amaya:** Yo creo que sería exitoso en la medida de que el entrevistado, en este caso el candidato a Asamblea, sea sincero, sea honesto, se mantenga en un ambiente agradable de conversación y pueda hacer llegar a la comunidad, a la población, un mensaje de conciencia y de reflexión para que voten por ellos. Cualquiera que sea. 

**Anfitrión de oportunidad:** ¿Cuál es el estilo que tú buscas al inquirir esta información de la gente que se candidatiza? O sea, tú vas a ser confrontacional, o tu interés es buscar el lado humano de la persona candidata, o vas a requerir sobre preguntas específicas de lo que es la realidad de la provincia. ¿Cuál es el enfoque que tú piensas darle a tu trabajo periodístico acá? 

**Lupita Amaya:** Como es conocido, yo no soy una periodista, yo no soy tampoco una política que sea conocida. Sin embargo, de ello soy una maestra, soy psicóloga. Y por tanto, siempre voy a estar pensando en el lado humano, del ser humano, porque quiero que yo mismo y la población pueda conocer ese lado de las personas. A veces se esconden detrás de un micrófono, detrás de una cámara, detrás de una campaña, personas que realmente son valiosas. Y quisiera conocerlas un poco más. No busco confrontar, no busco comparar, no busco tampoco disminuir a ningún candidato. Darle el espacio igualitario a todos. 

**Anfitrión de oportunidad:** Ahora, en tu criterio, ¿Qué debe tener un candidato para ganarse el derecho a representar a la provincia en la Asamblea Nacional? 

**Lupita Amaya:** Creo que debe tener primero ganas de servir, ¿No? Debe tener todo ese afán de servicio. Debe querer ir a la Asamblea. Entender. Creo que el candidato no debe ser alguien que crea que ya lo sabe todo. Si va con esa actitud, va a bloquearse ya y no vamos a tener un asambleista realmente que represente. 

**Anfitrión de oportunidad:** Van a tener mandato, un mandato recortado, dos años o algo así. No se puede esperar de que desarrollen iniciativas legislativas demasiado importantes porque no van a tener tiempo para hacerlo. En tu punto de vista: ¿Cuál sería una participación exitosa de una persona dentro de la Asamblea dada esta restricción? 

**Lupita Amaya:** Hay proyectos que están ya en proceso, ya han tenido primer análisis, segundo análisis, y que de pronto son leyes que van a tener que pasar y ser aprobadas, que si es que no se ponen de acuerdo, si es que no van a tomar el camino de continuar. Van a tener una asamblea en la que todo mundo va a querer iniciar, todo mundo va a querer tener una ley nueva y no van a hacer nada. Entonces yo sí creo y considero que deben ir dispuestos a ponerse la camiseta del país y tienen que ir dispuestos a apoyar las iniciativas que sean posibles. Y una cosa que también es importante que en este nuevo periodo los asambleistas vayan a dar gobernabilidad. Porque hemos visto un país confrontado en estos dos últimos años, en el que han estado tan confrontados que no ha habido posibilidades de gobernabilidad. Por eso estamos en este momento con una muerte cruzada. Porque no hubo esa capacidad de buscar consenso, de buscar acercamientos. Entonces, independientemente de la camiseta que lleves, tienes que hacer lo posible por continuar revisando, estudiando leyes y aportando para la gobernabilidad del país.

**Anfitrión de oportunidad:** La capacidad de diálogo de una persona debe ser lo suficientemente poderosa como para poder sobrepasar las directrices partidarias. ¿Cómo tú ves que una persona que ejerce la política en este ámbito parlamentario puede conciliar ambas cosas? Por un lado, el aspecto de la debida disciplina partidaria y por otro lado, una actitud deliberante, una actitud dialogante y una actitud que busca encontrar los puntos medios que permitan avanzar al país. 

**Lupita Amaya:** Es bastante complejo si es que desde las propias directivas de los partidos tienen ya la posición de confrontar porque si bien es cierto, en este corto tiempo va a haber mucha gente que va a estar trabajando, no precisamente para la gobernabilidad, pienso yo, sino más bien para la próxima candidatura. Y en ese proceso va a desprestigiarse mucho más el asambleísta, la asamblea misma, porque no van a estar pensando en el país, sino en las próximas elecciones. Entonces, va a estar muy complicado. Sin embargo, sí tendría esperanza de que el asambleísta o asambleísta que llegue, sea una persona capaz de poder incidir en su equipo, en su grupo. Y pueda por lo menos llegar a consensos que no es que marquen o hagan la gran diferencia, pero todo suma en el momento de las elecciones, en el momento de que sean votaciones, que sean decisiones que vayan a favor del pueblo. 

**Anfitrión de oportunidad:** ¿Cómo tú crees que contribuye Choling Entrevistas Políticas Asamblea 2023 para el buen logro de un parlamento dialogante y que dé gobernabilidad? ¿Cuál es la responsabilidad que tiene el podcast en este contexto? 

**Lupita Amaya:** ¿Sabes qué? Pienso que este espacio le va a permitir reflexionar. Reflexionar sobre su papel. Vamos a hablar de la gobernabilidad, vamos a hablar de su labor misma, de las funciones dentro de la Asamblea. Vamos a ver esa capacidad de incidir a nivel de equipo o de ser tan orgánica a su partido y dar o no la posibilidad de la gobernabilidad. Que se pongan la camiseta, o sea que la reflexión nos lleve a pensar en el país. Que no sea nada más un intento de llegar por llegar, por querer ser, por demostrar poder. Llegar a la Asamblea no es coger más poder para mí, sino es tener más obligación de servir. Eso es uno de los conceptos que para mí son importantes dentro de la política, es el servicio que debe tener un elegido por nosotros, tiene que estar al servicio de nosotros, no al servicio de sus intereses. 

**Anfitrión de oportunidad:** Ahora, el próximo lunes va a ser liberado el primer episodio de Choling, entrevistas políticas, elecciones a la Asamblea 2023. ¿Qué expectativas tienes tú con este lanzamiento? 

**Lupita Amaya:** Quisiera que sea escuchado. ¡Por supuesto! Mi expectativa es que sea escuchado. Tenemos barreras sí, tenemos barreras como la edad, que muchas personas de edad adulta no saben lo que es un podcast. Sin embargo, los jóvenes sí lo saben. Entonces tengo expectativas de que puedan ser escuchados por los jóvenes. Y que digan, ah, mira, esta asambleista me gusta porque por tal cosa y por tal cosa que se vuelven reflexivos y que sean personas que se metan dentro del mundo de la política de opinión. Porque así como vamos, vamos viendo como los partidos van envejeciendo y no van teniendo líderes porque no van formando gente, porque tampoco les interesa a los jóvenes hacer parte. Los partidos políticos están llenos de gente vieja. 

**Anfitrión de oportunidad:** Pero tu primera entrevistada de una persona joven, ¿No? Menos de 40 años. Se sentó cómodamente a hacer el podcast contigo, ¿Cómo fue esa experiencia? 

**Lupita Amaya:** La experiencia con la candidata asambleísta de Revolución Ciudadana fue interesante porque se dio un ambiente totalmente de confianza, de igualdad. O sea, no había la brecha generacional porque ella es una mujer joven y yo soy una mujer adulta ya. Entonces, no hubo esa diferencia, sentí que... se daba muy cómodamente la conversación. 

**Anfitrión de oportunidad:** ¿Qué es lo que tú deseas para la provincia de Santa Elena en esta legislatura, en lo que queda esta legislatura? ¿Qué aspiración tienes? 

**Lupita Amaya:** Mi aspiración es que vayan asambleistas que sean visibles. Porque hemos tenido candidatos que en la península son mucha palabra, son muy representativos, pero que cuando llegan a la asamblea, al Congreso se desaparecen, se invisibilizan. Yo sé que ahí juega también muchas veces el papel, la prensa, porque no a todo mundo los entrevistan, no les dan figura a todo mundo. Sin embargo, nosotros creo que a excepción de uno, que sí lo han entrevistado porque ha tenido participación en una ley muy importante. 

**Anfitrión de oportunidad:** ¿Y qué deseas para el país, para el Ecuador en esta legislatura? 

**Lupita Amaya:** Gente que sepa, ¿No? Que logren... vigilar, mantenerse vigilantes con que no pasen ninguna ley que vaya a afectar a las personas, a los más pobres del país. Que sean leyes que beneficien a todos, que no estén con dedicatoria hacia un sector que siempre lo ha podido, que es pudiente. 

**Anfitrión de oportunidad:** Ahora invita a la gente a que escuche Choling Entrevistas Políticas para la Asamblea 2023 que va ser irradiado por Radio La Voz de la península y por la plataforma de podcast que sea de la elección de cada escucha. 

**Lupita Amaya:** Bueno, a ustedes, chicos, chicas, en particular, sé que van a lograr escuchar un podcast con agrado y también motiven a sus padres. Hagan que sus padres entiendan que una forma de ir escuchando, aprendiendo es a través de la escucha, a través de la radio. Esta es la post radio, se dice. El podcast es la post radio porque usted... esté escuchando una voz y usted no necesita estar detenido frente a un video, sino que usted va a estar sentado, trabajando, caminando, haciendo sus actividades, manejando, andando en bicicleta, haciendo cosas que a usted le agrade escuchándonos. Y escuchándonos usted va a tener la capacidad de decidir la mejor opción para nuestra península. Escúchenos. 

**Anfitrión de oportunidad:** Bueno. Esa es la masterada Lupita Maya, anfitriona del podcast Choling Entrevistas Políticas para la Asamblea 2023. Sin falta escúchenla por La Voz de la Península el día lunes y a través de la plataforma de podcast de su elección.



+ Correo: <unicornioazul@disroot.org>
