---
layout: post  
title: "T1E2 ARISDELY PARRALES LISTA 5 REVOLUCION CIUDADANA"  
date: 2023-07-12  
categories: podcast  
image: images/T1E2_CHOLING_ARISDELY_PARRALES.jpg
image2: https://choling.gitlab.io/images/T1E2_CHOLING_ARISDELY_PARRALES_002.jpg
podcast_link: ia902701.us.archive.org/17/items/t-1-e-2-choling-arisdely-parrales/T1E2_CHOLING_ARISDELY_PARRALES.mp3
tags: [Arisdely Parrales, Revolución Ciudadana, Rafael Correa, Asamblea Nacional, Muerte cruzada]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E2_CHOLING_ARISDELY_PARRALES.jpg)

<audio controls>
    <source src="https://ia902701.us.archive.org/17/items/t-1-e-2-choling-arisdely-parrales/T1E2_CHOLING_ARISDELY_PARRALES.mp3" type="audio/mpeg">
    <script async data-type="track" data-hash="ePDBOReWZ5" data-track="3" src="https://app.fusebox.fm/embed/player.js"></script>
</audio>

**T1E1 ARISDELY PARRALES LISTA 5 REVOLUCION CIUDADANA**

**TRANSCRIPCION**

**Lupita Amaya:** Arisdely muchas gracias por estar en este día con nosotros, por haber aceptado la invitación. Quiero compartir contigo que yo no soy una profesional de la comunicación. Sin embargo, estoy en este momento puesta en los zapatos de los peninsulares, queriendo conocer a los candidatos para poder elegir el mejor. Quisiera que tú nos digas por qué crees tú que debes ser elegida asambleísta de la provincia de Santa Elena.

**Arisdely Parrales:** Muy buenos días, primero Lupita, muchísimas gracias por la invitación que nos ha hecho a este programa, que como usted bien dice es como un ciudadano, ¿no? Desde el ciudadano querer saber quiénes son los candidatos en este momento para conformar la nueva Asamblea Nacional y así pues le agradezco que nos haya hecho esta invitación y estamos muy gustosos de estar aquí. 
¿Por qué creemos que la lista de la revolución ciudadana, RC5, debe ser electa en estas elecciones para formar una asamblea nacional y ser gobierno?
 Porque también tenemos nuestro binomio presidencial a la cabeza, Luisa González y Andrés Arauz. Creemos que, en primer lugar, porque somos un proyecto político que ya demostramos que podíamos sacar adelante al país. Somos un proyecto político que ya demostramos en la década ganada que velábamos por el bienestar de todos y de todas. Somos un proyecto político que ya, y que la gente lo recuerda, que todos los ecuatorianos recordamos que antes estábamos mejor y no lo decimos como una frase de marketing, ¿no? De marketing político. Lo decimos con la convicción de que fue una realidad. Que fue un momento en la historia del ecuador en la que se vivieron grandes cambios, no solamente a nivel político, sino también a nivel social. Recordemos las escuelas del milenio, toda la inversión que se hacía en salud, en seguridad, en educación y en fomento productivo también para... 

**Lupita Amaya:** Arisdely, una preguntita. Cuando tú me hablas de proyecto político, estamos hablando de revolución ciudadana, nivel nacional. 10 años década ganada. Yo quisiera ahora que tú te enfoques en la provincia de Santa Elena. ¿Qué proyecto político tenemos nosotros en mente? ¿Qué es lo que estamos hablando, pensando como asambleísta? Así es que llegarás a ser elegida. ¿Qué proyecto político tienes en mente? 

**Arisdely Parrales:** Bueno, pues es conformar un gran bloque que permita. Eso va a permitir que tanto en Santa Elena como en el Ecuador haya gobernabilidad y se puedan invertir recursos como ya se lo hacía antes en nuestra provincia. Tal vez un asambleísta no hace obras, no tiene esas facultades y competencias, pero lo que sí sabemos es que seremos un gobierno. Y un gobierno como tal necesita una estabilidad, una estabilidad que no la teníamos. No había gobernanza en el país. Entonces, al ser nosotros, formar parte de este proyecto político, estamos asegurando que en nuestra provincia también haya una gobernabilidad y haya un gobierno que permita que sigamos creciendo como provincia. 

**Lupita Amaya:** ¿Estaría dispuesta como asambleísta a hablar con otros grupos políticos? ¿Estarías dispuesta a ceder en algunas posiciones en caso de que sea para el bien de la provincia y del país?

**Arisdely Parrales:** Recordemos que este es un proyecto político nacional y de bancada. Habrá que tomar las diferentes decisiones al interior de la bancada del Movimiento Revolución Ciudadana. Se debatirán. Hay una democracia que se debate dentro de la bancada. Y una vez que se toman las decisiones, las decisiones se toman y se respetan y se vota en bloque. Ahora lo que usted menciona, que claro, siempre se necesita encontrar mínimos comunes con otras ideologías, con otras bancadas de otras tal vez partidos políticos o movimientos políticos, eso se lo hará de manera consensuada al interior y de manera orgánica. al interior de RC.

**Lupita Amaya:** Muy bien. Hay una cosa que me queda en el ambiente. Tú eres maestra, ¿no? 

**Arisdely Parrales:** Así es. 

**Lupita Amaya:** Tú eres de... Vienes de una tradición de docentes. Tu padre fue docente. Tú eres docente. Me gustaría tú que... ¿Cómo podrías incidir en alguna ley, en alguna cuestión que tenga que ver con mejorar la calidad educativa? Nosotros tenemos en este momento una situación de educación fiscal en particular en abandono. ¿Cómo tú incidirías, cómo tu papel de liderazgo como para poder incidir en este tema que es tuyo? Porque como te digo, vienes de la tradición educativa de tu padre, de tu familia, tienes un colegio. ¿Cómo harías tú para ayudar a mejorar la calidad educativa?

**Arisdely Parrales:** Recordemos que fue en el gobierno de la Revolución en donde se aprobó la Ley que hoy está vigente, la Ley de educación intercultural y con esto vinieron grandes cambios y vinieron grandes desafíos que se estaban mejorando para el bienestar de la comunidad educativa ecuatoriana. Ahora, la educación no es estática, la educación es evolución constante. Entonces en base a eso y también la educación tiene muchas Arisdely Parrales: tas. Tenemos el rol de los docentes que es fundamental para la comunidad educativa. Tenemos el rol de los directivos, tenemos el rol de los padres de familia, el rol de los estudiantes y el rol también de un entorno. Entonces ahí viene el desafío y también viene el compromiso de estar cercanos a nuestro entorno educativo. Somos docentes, somos maestros de vocación, de historia, familia, y es lo que hacemos, ¿no? El compromiso estará en recoger, en estar cercanos a la comunidad educativa y poder recoger cuáles son aquellas situaciones que a través de una ley se puedan mejorar. Adora, decir que vamos a en este corto tiempo, verdad, porque recordemos que esta asamblea estará siendo electa para terminar el periodo legislativo actual que termina o fenece en el 2025 estar hablando de grandes y un gran proyecto que podamos llevar. No somos, no somos personas que decimos cosas que tal vez no vayamos a poder lograr.

**Lupita Amaya:** Hay cosas que de pronto sí se podría buscar alguna forma de ayuda. Por ejemplo, yo veo que en los barrios del mercado, por el sector del mercado, hay mucha droga, hay mucho consumo de droga. He estado pensando qué se puede hacer aquí en el sector porque no es una problemática local. Sin embargo, ubicándonos en la problemática local, qué hacer con tanto consumo de droga, con el abandono que tienen las alcaldías. ¿Tú crees de alguna forma lograrías consensuar con los alcaldes para lograr un proyecto común aquí en la península? Porque es lo que hace falta. Cada quien lleva agua para su molino. La Libertad es una cuestión pequeña porque estamos en este momento en La Libertad. Sin embargo vemos los problemas sociales evidentes en la calle, aquí mismo, en el mercado, en todos lados los crímenes que se han dado también en este sector. ¿Habría alguna forma de incidir en este tema del consumo de drogas en ese sector? 

**Arisdely Parrales:** Recordemos que el tema del consumo de drogas es un tema de salud pública. Son personas y seres humanos que lamentablemente han caído en adicción y que lógicamente pues terminan muchas veces, terminan con sus días viviendo en adicción. Entonces recordemos que era en el gobierno de la revolución ciudadana donde el ser humano, ¿verdad?, era visto como el principal objetivo del accionar público y político. Entonces estamos seguros que una vez que seamos gobierno con Luisa y con Andrés, las políticas públicas que sí existen, tanto por ejemplo el Mies, el Ministerio de Salud Pública y otros ministerios, se unan para poder hacer acciones de rehabilitación de todas estas personas que han caído en el tema de drogas. Recordemos que esto es un asunto de salud pública también. Y que con una mejor atención en salud, que sí existen los protocolos, lo que no existe es la asignación de recursos para poder rehabilitar a todas estas personas.

**Lupita Amaya:** Hay una cosa que me gustaría que se ponga en el mapa de Santa Elena. Nosotros en algún momento tuvimos un trabajo, un proyecto en el sector de Julio Moreno y realmente para nosotros Julio Moreno es la oreja olvidada de la provincia de Santa Elena. Es como una oreja, tú vienes en línea recta hacia Salinas y de repente tú olvidas que existe esa oreja que se mete hacia Julio Moreno, todo el sector de Limoncito, todo ese sector. Es un sector que ojalá si tú llegas a la Asamblea se pueda luchar por algunas cuestiones o presentar algún proyecto en el que se le ponga el mapa a ese sector. Tenemos en el sector de allá hay agua, agua no. No de río, pero tenemos ahí el canal,

**Arisdely Parrales:** El trasvase, sí.

**Lupita Amaya:** El trasvase, que es lo que ayuda mucha gente en este momento para sembrar, porque es un sector olvidado. Ojalá. ¿Tú conoces Julio Moreno?

**Arisdely Parrales:** Claro que sí, hemos tenido la oportunidad de estar en toda la provincia de Santa Elena, conocemos Julio Moreno y todas las comunas, parroquias. Tuvimos esa gran oportunidad cuando fuimos parte de la gobernación de la Provincia de Santa Elena en el año 2014, 2016, perdón, y tuvimos la oportunidad de conocer todo el territorio santelenense. Y lo que usted menciona, pues, se necesita ahí, es más bien una voluntad de los gobiernos autónomos descentralizados. Que estamos seguros que sí hay inversión en la zona, pero que hacen falta más.

**Lupita Amaya:** Ahora, tú me hablas al propósito de la militancia. ¿Qué tiempo estás en Revolución Ciudadana? ¿Cómo ha sido tu crecimiento en Revolución Ciudadana para poder llegar en este momento a ser la representante de esa línea política, de ese proyecto político? ¿Cómo ha sido tu crecimiento en la militancia allí?

**Arisdely Parrales:** Bueno, nosotros empezamos desde muy jovencitos. En mi familia también, aparte de la educación, pues ha sido la política parte también de nuestra formación en casa. Y bueno, pues luego confiamos en el proyecto de la Revolución Ciudadana, a la cabeza Rafael Correa, nos adherimos, hicimos militantes en su momento, y empezamos, ¿no? Empezamos junto a mi padre, junto a nuestra familia, a llevar el mensaje de la Revolución Ciudadana, recordemos que en el año 2008 aprobamos una nueva constitución y para eso pues teníamos que socializarlo.

**Lupita Amaya:** O sea, tú estás desde jovencita.

**Arisdely Parrales:** Desde muy jovencita, ya son más de 15 años podría decir que hemos estado militando en Revolución Ciudadana. Y desde ahí pues hemos llevado un camino muy bonito, muy enriquecedor y también un camino en el cual crecimos políticamente. Y no hablo de políticamente solo en un tema de candidatura, sino en el tema ideológico. Pertenecimos a la J-PAIS, a la J-35. activamos los jóvenes, las escuelas de formación política dentro de la provincia de Santa Elena, en nuestro cantón. Activamos la organización como jóvenes que también recordamos que es en el gobierno de la revolución ciudadana donde a los jóvenes se nos dio papeles fundamentales y protagonistas, no solamente como parte de la militancia sino también como parte de gobierno. Hay muchas caras y rostros jóvenes que también formaron parte de la administración pública.

**Lupita Amaya:** O sea, veo de acuerdo a tu relato que la formación se ha ido dando paulatinamente desde muy jovencita. Una pregunta, ¿Tú te irías a vivir a Quito? 

**Arisdely Parrales:** Bueno, pues mi casa siempre va a ser Santa Elena. Mi casa siempre va a ser la provincia de Santa Elena. Lógicamente, ya al tener un accionar en Quito, pues vamos a tener que modificar un poco nuestra cotidianidad. Pero también hay las ventajas de los medios de transportes que te permiten estar en un lado y en otro lado de manera rápida. Entonces mi casa será Santa Elena, siempre, y tendremos que estar en la Asamblea Nacional cumpliendo con todo el rol que esto amerita, con toda la responsabilidad también, pero retornaremos siempre a Santa Elena.

**Lupita Amaya:** Sí, me parece muy bien eso. Porque esta pregunta iba porque se ha conocido de asambleístas que no van, que simplemente se eligen y mandan a su reemplazo, por decir algo, ¿no? Y cuando les toca ya trabajar en el tema, no están. Entonces, como que la Asamblea se los come, desaparecen. Hemos tenido representantes de la península que desaparecen en el... 

**Arisdely Parrales:** En el territorio.

**Lupita Amaya:** No, no en el territorio, sino desaparecen ya en la Asamblea misma. Son personas que se vuelven invisibles. Entonces, ¿cómo harías, Arisdely Parrales: delys, para que allá tu voz se escuche? ¿Tu voz sea una voz que sea considerada como la voz de Santa Elena? ¿Cómo harías tú para que eso?

**Arisdely Parrales:** Con la capacidad que tenemos las mujeres santelenenses. Con esa convicción de que al recibir el voto popular, al recibir la confianza de los santelenenses, pues es nuestra responsabilidad ser voz, ser su voz en este espacio que sería el espacio legislativo. Tenemos muchos temas pendientes. Por ejemplo, está pendiente la aprobación de las reformas a la ley de comunas, por ejemplo. Que es una ley que ya está obsoleta, que todo en esta vida evoluciona. Y hay ya un trabajo realizado también ahí. Solo hace falta su socialización, su posterior aprobación. Entonces, en todas estas situaciones que forman parte de la cotidianidad de Santa Elena y que son importantes para Santa Elena, estaremos seguros que con la capacidad que tiene la mujer santelennense y con las ganas que tenemos de servir a nuestra ciudadanía, pues lo vamos a hacer.

**Lupita Amaya:** Me gusta mucho esas palabras de la capacidad de la mujer santelennense. Yo te deseo lo mejor, Arisdely, que si sales asambleísta que se logren concretar todos los proyectos que tengas. y sacar adelante a aquellos que están dormidos, como lo que tú acabas de decir, la ley de las comunas, porque nuestra península se caracteriza por las comunas, porque están organizadas por comunas, porque los terrenos, los territorios están divididos en comunas. Entonces yo creo que va a ser una cosa muy importante. Unas palabras para despedirte de nuestro conversatorio. 

**Arisdely Parrales:** Lupita, en primer lugar agradecerles a ustedes por este espacio que es importante llevar a la ciudadanía el mensaje desde diferentes aristas y lugares. Así que yo les agradezco a ustedes muchísimo por la oportunidad de podernos expresar desde sus micrófonos, desde este espacio nuevo y novedoso que va...

**Lupita Amaya:** Choling

**Arisdely Parrales:** Choling aquí en la provincia de Santa Elena y que me parece espectacular. Y así mismo a toda la ciudadanía que nos escucha, a los jóvenes. Tal vez los jóvenes, muy jovencitos ahora, no tengan tal vez la claridad y el recuerdo de lo que vivimos en la revolución ciudadana. Pero hacia allá va nuestro mensaje, queridos jóvenes. El gobierno de la revolución ciudadana es el gobierno que devolvió a la patria su dignidad y estamos seguros que a partir de agosto con el voto de todos, con la importancia y la fuerza y la alegría de los jóvenes, que se va a ver reflejado en el voto popular, pues volveremos a ser patria. Volveremos a tener un gobierno que se preocupe por todos y por todas. Así que a toda la ciudadanía santaelenense, a los jóvenes, a las madres, a los pescadores, a los comuneros, a todos quienes hacemos día a día grande a nuestra provincia Santa Elena, vamos juntos, vamos juntos a votar por el gobierno de la revolución ciudadana, a votar por este proyecto político que muchos, muchos recordamos que fue el proyecto que le devolvió a la patria sus días de engrandecimiento. Que sí, tuvimos un periodo abrupto en el cual nuestro desarrollo se vio truncado y acabamos de vivir un desastre. Pero estamos seguros que a partir de agosto y con la posesión de nuestra presidenta, Luisa González y Andrés Arauz, pues vamos a volver a tener patria.

**Lupita Amaya:** Otra vez: Muchas gracias.

**Arisdely Parrales:** Muchas gracias.


+ Correo: <unicornioazul@disroot.org>
