---
layout: post  
title: "TRAILER"  
date: 2023-06-22  
categories: podcast  
image: images/T1E2_EL_TEATRO.jpeg
podcast_link: ia902601.us.archive.org/24/items/trailer-choling/TRAILER_CHOLING.mp3
tags: [ELECCIONES 2023, Asamblea Nacional, Peninsula, Santa Elena]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E2_EL_TEATRO.jpeg)

<audio controls>
    <source src="https://ia902601.us.archive.org/24/items/trailer-choling/TRAILER_CHOLING.mp3" type="audio/mpeg">
    <script async data-type="track" data-hash="ePDBOReWZ5" data-track="3" src="https://app.fusebox.fm/embed/player.js"></script>
</audio>

**TRAILER**

Bienvenidos a "Choling", el podcast donde a partir de nuestra propia necesidad de información exploraremos el panorama político de la Provincia de Santa Elena.

Soy Lupita Amaya, y te invitamos a adentrarnos en un mundo fascinante de ideas, propuestas y perspectivas políticas.

En esta temporada única, nos enfocaremos en las elecciones anticipadas del año 2023 y entrevistaremos a candidatos y candidatas peninsulares a Asambleístas.

Acompáñanos mientras analizamos los desafíos, sus propuestas y las visiones de los líderes políticos que buscan representar a la comunidad peninsular en este momento crucial de la historia.





Nos puedes escribir a:

+ Correo: <unicornioazul@disroot.org>
