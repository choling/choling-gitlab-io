---
layout: page
title: "Sumario"
permalink: /about/
image: images/ Don_Miguel.jpg
comments: true 
---

![#piloto]({{ site.baseurl }}/images/Don_Miguel.jpg)


El 17 de mayo de 2023, el presidente de Ecuador, Guillermo Lasso, tomó una decisión histórica: disolver el Congreso y convocar a elecciones anticipadas. Esta medida, que nunca se había aplicado antes en el país, se conoce como “muerte cruzada” y está contemplada en la Constitución de 2008 e implicó elecciones anticipadas para Presidente y Asambleistas.

Los plazos para dar a conocer las propuestas son tan ajustados que se quedan cortos, muchas personas votarán con menos información que en oportunidades anteriores.

Don Miguel Amaya dueño del Bazar Lupita de La Libertad, le pregunta a su hija la Masterada Lupita Amaya Jacho por quién votar.

Lupita tampoco conoce y decide preguntarle a los candidatos y candidatas peninsulares 2023 por sus ideas y compromisos con la península de Santa Elena.

Esto es Choling.

Gracias por escuchar
